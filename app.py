from flask import Flask, render_template

app=Flask(__name__)

@app.route('/') #главная страничка "декоратор"
def print_hello():
    return render_template("index.html")

@app.route('/text') #главная страничка "декоратор"
def print_text():
    return "texttexttexttext"

def print_text2():
    return "texttexttexttex222t"

app.run()